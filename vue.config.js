module.exports = {
     css: {
         loaderOptions: {
             scss: {
                 prependData: `
                 @import "@/assets/css/global/attribute.scss";
                 @import "@/assets/css/global/page-size.scss";
                 @import "@/assets/css/global/tag.scss";
                 @import "@/assets/css/global/components.scss";`
             }
         }
     },
     pluginOptions: {
        electronBuilder: {
          builderOptions: {
              "appId": "com.ykxx.app",
              "productName":"Hacking-Tools",//项目名，也是生成的安装文件名，即aDemo.exe
              "copyright":"Copyright all",//版权信息
              "win":{//win相关配置
                  "target": [
                      {
                          "target": "nsis",//利用nsis制作安装程序
                          "arch": [
                              "x64",//64位
                          ]
                      }
                  ]
              },
              "nsis": {
                  "oneClick": false,
                  "createDesktopShortcut": "always",
                  "allowToChangeInstallationDirectory": true
              }
          }
        }
      }
}