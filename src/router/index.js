import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)
function getPage (pageName) {
  return () => import(`@/views/${pageName}/index.vue`);
}
const routes = [
  {
    path: '/',
    name: '/',
    component: getPage('C-V-S')
  }, {
    path: '/uas',
    name: 'uas',
    component: getPage('U-A-S')
  }, {
    path: '/fas',
    name: 'fas',
    component: getPage('F-A-S')
  }, {
    path: '/cie',
    name: 'cie',
    component: getPage('C-I-E')
  }, {
    path: '/aet',
    name: 'aet',
    component: getPage('A-E-T')
  }, {
    path: '/psi',
    name: 'psi',
    component: getPage('P-S-I')
  }, {
    path: '/hps',
    name: 'hps',
    component: getPage('H-P-S')
  }, {
    path: '/avu',
    name: 'avu',
    component: getPage('A-V-U')
  }, 
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
