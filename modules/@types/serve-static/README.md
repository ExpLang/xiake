# Installation
> `npm install --save @types/serve-static`

# Summary
This package contains type definitions for serve-static (https://github.com/expressjs/serve-static).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/serve-static.

### Additional Details
 * Last updated: Tue, 28 Jul 2020 07:06:38 GMT
 * Dependencies: [@types/express-serve-static-core](https://npmjs.com/package/@types/express-serve-static-core), [@types/mime](https://npmjs.com/package/@types/mime)
 * Global values: none

# Credits
These definitions were written by [Uros Smolnik](https://github.com/urossmolnik), and [Linus Unnebäck](https://github.com/LinusU).
