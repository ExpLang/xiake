## [3.0.1](https://github.com/thenativeweb/boolean/compare/3.0.0...3.0.1) (2020-02-11)


### Bug Fixes

* Simplify comparison code to not use unicode regexp flag ([#99](https://github.com/thenativeweb/boolean/issues/99)) ([2be2aeb](https://github.com/thenativeweb/boolean/commit/2be2aeb244c060eccb388dacc6903bbad193e745))
