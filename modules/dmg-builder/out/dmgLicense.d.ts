import { PlatformPackager } from "app-builder-lib";
export declare function addLicenseToDmg(packager: PlatformPackager<any>, dmgPath: string): Promise<string | null>;
